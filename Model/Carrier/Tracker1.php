<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Annex\CarrierTracker\Model\Carrier;

use Annex\CarrierTracker\Model\Carrier\AbstractCarrier as AC;
use Magento\Shipping\Model\Carrier\CarrierInterface;

class Tracker1 extends AC implements CarrierInterface
{
    protected $_code = 'tracker1';
    
    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return ['tracker1' => $this->getConfigData('name')];
    }
}
