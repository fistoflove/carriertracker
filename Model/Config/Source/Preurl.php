<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Annex\CarrierTracker\Model\Config\Source;

class Preurl implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Annex\CarrierTracker\Model\Carrier\Tracker1
     */
    protected $_carrierTracker;

    /**
     * @param \Annex\CarrierTracker\Model\Carrier\Tracker1 $carrierTracker1
     */
    public function __construct(\Annex\CarrierTracker\Model\Carrier\Tracker1 $carrierTracker1)
    {
        $this->_carrierTracker = $carrierTracker1;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $arr = [];
        foreach ($this->_carrierTracker->getCode('preurl') as $k => $v) {
            $arr[] = ['value' => $k, 'label' => $v];
        }
        return $arr;
    }
}
