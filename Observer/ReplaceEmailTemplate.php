<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Annex\CarrierTracker\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;


/**
 * Tracker observer to replace the email template
 */
class ReplaceEmailTemplate implements ObserverInterface
{
    /**
     * @var \WebShopApps\Tracker\Helper\Data
     */
    protected $_trackerDataHelper;

    /**
     * ReplaceEmailTemplate constructor.
     * @param \WebShopApps\Tracker\Helper\Data $shipperDataHelper
     */
    public function __construct(
        \Annex\CarrierTracker\Helper\Data $shipperDataHelper
    )
    {
        $this->_trackerDataHelper = $shipperDataHelper;
    }
    /**
     * Record order shipping information after order is placed
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        if ($this->_trackerDataHelper->getConfigValue('carriers/tracker1/active')) {

            $blockentity = $observer->getBlock();
            if ($blockentity->getTemplate() == 'Magento_Sales::email/shipment/track.phtml') {
                $blockentity->setTemplate('Annex_CarrierTracker::email/shipment/track.phtml');
            }
        }
    }

}

