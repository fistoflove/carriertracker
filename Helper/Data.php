<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Annex\CarrierTracker\Helper;

use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Tracker data helper
 */
class Data extends  \Magento\Framework\App\Helper\AbstractHelper
{
    
    /**
     * Get Config Value
     *
     * @param $configField
     * @return mixed
     */
    public function getConfigValue($configField)
    {
        return $this->scopeConfig->getValue($configField,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

    }



}