<?php

namespace Annex\CarrierTracker\Plugin\Order\Shipment;


class TrackPlugin {

    public function afterGetNumberDetail(\Magento\Shipping\Model\Order\Track $trackObj, $result) {


        if (strpos($this->getCarrierCode(),'tracker')!== false && 
            strpos($result, 'No detail for number') !== false) {

            $carrierInstance = $this->_carrierFactory->create($this->getCarrierCode());
            if (!$carrierInstance) {
                $custom = [];
                $custom['title'] = $this->getTitle();
                $custom['number'] = $this->getTrackNumber();
                return $custom;
            } else {
                $carrierInstance->setStore($this->getStore());
            }

            $rplChars = array(" " => '');
            $string = $this->getShipment()->getShippingAddress()->getPostcode();
            $postcode = strtr($string,$rplChars);
            
            $trackingInfo = $carrierInstance->getTrackingInfo($this->getNumber(), $postcode);
            if (!$trackingInfo) {

                return Mage::helper('sales')->__('No detail for number "%s"', $this->getNumber());
            }

            return $trackingInfo;
        }
        return $result;

    }

}